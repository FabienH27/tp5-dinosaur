﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Horde
    {
        private List<Dinosaur> dinosaurs;

        public Horde()
        {
            this.dinosaurs = new List<Dinosaur>();
        }
        public void addDinosaur(Dinosaur dino)
        {
            this.dinosaurs.Add(dino);
        }

        public void removeDinosaur(Dinosaur dino)
        {
            this.dinosaurs.Remove(dino);
        }

        public string showAllDinosaurs()
        {
            string result = string.Empty;
            foreach (Dinosaur dino in this.dinosaurs)
            {
                result = string.Format("{0}\n- {1}", result, dino.sayHello());
            }
            return result.Trim();
        }

        public List<Dinosaur> GetDinosaurs()
        {
            return this.dinosaurs;
        }
    }
}
