﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Diplodocus : Dinosaur
    {
        public override string Specie { get { return "Diplodocus"; } }
        public Diplodocus(string name, int age) : base(name, age)
        {

        }
    }
}
