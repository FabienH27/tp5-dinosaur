﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Stegosaurus : Dinosaur
    {
        public override string Specie { get { return "Stegosaurus"; } }
        public Stegosaurus(string name, int age) : base(name, age)
        {
            
        }
        public override string ToString()
        {
            // We only call base ToString from Object class Here
            return base.ToString();
        }
    }
}
