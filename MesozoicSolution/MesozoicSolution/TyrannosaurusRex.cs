﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class TyrannosaurusRex : Dinosaur
    {
        public override string Specie { get { return "Tyrannosaurus-Rex"; } }
        public TyrannosaurusRex(string name, int age) : base(name, age)
        {

        }
    }
}
