﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Laboratory
    {

        public static Diplodocus createDiplodocus(string name)
        {
            return new Diplodocus(name, 0);
        }

        public static Triceratops createTriceratops(string name)
        {
            return new Triceratops(name, 0);
        }

        public static Stegosaurus createStegosaurus(string name)
        {
            return new Stegosaurus(name, 0);
        }

        public static TyrannosaurusRex createTyrannosaurusRex(string name)
        {
            return new TyrannosaurusRex(name, 0);
        }
    }

}
