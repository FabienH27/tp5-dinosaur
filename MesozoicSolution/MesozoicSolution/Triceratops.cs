﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MesozoicSolution
{
    public class Triceratops : Dinosaur
    {
        public override string Specie { get { return "Triceratops"; } }
        public Triceratops(string name, int age) : base(name, age)
        {

        }
    }
}
