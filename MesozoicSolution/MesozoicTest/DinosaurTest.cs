﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using MesozoicSolution;

namespace MesozoicTest
{
    [TestClass]
    public class DinosaurTest
    {
        /*[TestMethod]
        public void TestDinosaurConstructor()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);

            Assert.AreEqual("Louis", louis.name);
            Assert.AreEqual("Stegausaurus", louis.specie);
            Assert.AreEqual(12, louis.age);
        }

        [TestMethod]
        public void TestDinosaurRoar()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Grrr", louis.Roar());
        }

        [TestMethod]
        public void testDinoAge()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual(12, louis.GetAge());
        }

        [TestMethod]
        public void TestDinosaurSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegausaurus", 12);
            Assert.AreEqual("Je suis Louis le Stegausaurus, j'ai 12 ans", louis.SayHello());
            throw new NotImplementedException();
        }

        [TestMethod]
        public void TestDinosaurHug()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Assert.AreEqual("Je suis Louis et je fais un calin a Nessie", louis.Hug(nessie));
        }

        [TestMethod]
        public void AddDinoTest()
        {
            Horde hordeTest = new Horde();
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            hordeTest.AddDino(louis);
            Assert.AreEqual("Louis", hordeTest.dinosaurs[0].name);
        }
        [TestMethod]
        public void RemoveDinoTest()
        {
            Horde hordeTest = new Horde();
            Dinosaur nessie = new Dinosaur("Nessie", "Diplodocus", 11);
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            hordeTest.AddDino(louis);
            hordeTest.AddDino(nessie);
            hordeTest.RemoveDino(louis);
            Assert.AreEqual(1, hordeTest.dinosaurs.Count);
        }
        [TestMethod]
        public void DinosSayHello()
        {
            Dinosaur louis = new Dinosaur("Louis", "Stegosaurus", 12);
            String[] dinoHello = new string[] { "Je suis Louis le Stegosaurus, j'ai 12 ans"};
            Horde hordeTest = new Horde();
            String[] actual;
            actual = hordeTest.dinosSayHello(louis);
            CollectionAssert.AreEqual(dinoHello, actual);
            Horde hordeTest = new Horde();

            foreach(Dinosaur dino in hordeTest.dinosaurs)
            {
                Assert.AreEqual(dinoHello, hordeTest.DinosSayHello(louis));
            }
        }

        [TestMethod]
        public void LaboTest()
        {
            Dinosaur Trex = new Dinosaur("Trex", "Tyranosaurus-Rex", 14);
            Assert.AreEqual(Trex, Laboratoire.CreateDinosaur("Trex", "Tyranosaurus-Rex", 14));
        }*/

        Dinosaur dinosaur = new Stegosaurus("Louis", 12);
        Stegosaurus dinosaur2 = new Stegosaurus("Louis", 12);

        [TestMethod]
        public void testOverride()
        {
            Assert.AreEqual("name : Louis, age : 12", dinosaur.ToString());   
        }

        [TestMethod]
        public void testEquals()
        {
            Assert.AreEqual(true, dinosaur.Equals(dinosaur2));
        }

    }
}
